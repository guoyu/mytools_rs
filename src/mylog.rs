#[test]
fn test_log() {
    println!(
        "etax_get_data::headless_chrome-0.9.0::browser::process:163 [2020-01-14 12:43:23\
         .508634] INFO \n\n Started Chrome. PID: 24976"
    );
    println!(r##"etax_get_data\headless_chrome-0.9.0\src\browser\process.rs:163"##);
}

/// 初始化日志
/// #[macro_use]
/// extern crate log;
/// extern crate chrono;
/// extern crate env_logger;
/// code: info!("{}", "info msg");
pub fn init_log(arg_log: Option<String>) {
    use flexi_logger::{Age, Cleanup, Criterion, Naming};

    let log = if let Some(v) = arg_log {
        v
    } else {
        String::from("info")
    };
    let log = log.as_str();
    std::env::set_var("RUST_LOG", log);
    std::env::set_var("actix_server", log);
    std::env::set_var("actix_web", log);

    flexi_logger::Logger::with_env_or_str(log)
        .log_to_file()
        .directory("logs")
        .format(|w, now, record| {
            let msg = format!(
                "[{}] {} {}:{} {}",
                now.now().format("%Y-%m-%d %H:%M:%S%.6f"),
                record.level(),
                record.file().unwrap_or("<unnamed>"),
                record.line().unwrap_or(0),
                &record.args(),
            );
            println!("{}", &msg);
            write!(w, "{}", msg)
        })
        .print_message()
        .rotate(
            Criterion::Age(Age::Day),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(30),
        )
        .start()
        .unwrap();
    //env_logger::init();
    warn!("start {} log", log);
    // env_logger::init();
}
