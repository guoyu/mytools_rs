use crate::{IResult};

/// prefix = "xxx-temp"
pub fn create_temporary_folder(prefix: String) -> IResult<String> {
    let dir = std::env::temp_dir();
    let uuid1 = uuid::Uuid::new_v4();
    let mut uuid_str = uuid1.to_string();
    uuid_str = uuid_str.replacen("-", "", 4);
    let dir_str = dir.to_str().unwrap();
    let mut dir_str = dir_str.to_string();
    dir_str.push_str(&prefix);
    dir_str.push_str("-");
    dir_str.push_str(uuid_str.as_ref());
    let _ = std::fs::create_dir(dir_str.clone());
    Ok(dir_str)
}

#[test]
fn test_create_temporary_folder(){
    let result = create_temporary_folder(String::from("test_wrwe"));
    println!("{:?}", result);
}
