
/// 获得现在的时候戳
pub fn get_time() -> (u128, u64) {
    let time = std::time::SystemTime::now();
    let time = time.duration_since(std::time::UNIX_EPOCH).unwrap();
    (time.as_millis(), time.as_secs())
}

/// 获得现在的时候戳
pub fn get_now_time_string() -> (String, String) {
    use chrono::FixedOffset;
    use chrono::TimeZone;
    use chrono::Utc;
    use chrono::{Datelike, Timelike};

    let millis = Utc::now().timestamp_millis();
    let date = FixedOffset::east(8 * 3600).timestamp_millis(millis);

    let year = date.year();
    let month = date.month() as u8;
    let day = date.day() as u8;
    let h = date.hour() as u8;
    let m = date.minute() as u8;
    let s = date.second() as u8;

    let month = get_zoer_val(month);
    let day = get_zoer_val(day);
    let h = get_zoer_val(h);
    let m = get_zoer_val(m);
    let s = get_zoer_val(s);

    let val1 = format!("{}-{}-{}", &year, &month, &day);
    let val2 = format!("{} {}:{}:{}", &val1, &h, &m, &s);
    (val1, val2)
}

pub fn get_zoer_val(v: u8) -> String {
    let v_str = v.to_string();
    if v_str.len() == 1 {
        "0".to_string() + v_str.as_ref()
    } else {
        v_str
    }
}

#[test]
pub fn test_get_now_time_string() {
    println!("{:?}", get_now_time_string());
}

/// 获得当前月份
pub fn this_month() -> (String, String) {
    use chrono::Utc;
    use chrono::Datelike;
    let date = Utc::now();
    let year = date.year() as u8;
    let month = date.month() as u8;
    let day = date.day() as u8;
    let begin = format!("{}-{}-01", year, get_zoer_val(month));
    let end = format!("{}-{}-{}", year, get_zoer_val(month), get_zoer_val(day));
    (begin, end)
}

#[test]
fn test_this_month() {
    let mounth = this_month();
    println!("mounth: {:?}", mounth);
}

/// 获得上个月的日期范围
pub fn last_month() -> (String, String) {
    let mut tuple1 = date_time::date_tuple::Date::today();
    tuple1.subtract_months(1);
    let year = tuple1.get_year();
    let month = tuple1.get_month();
    let begin = format!("{}-{}-01", year, get_zoer_val(month));

    let mut end_day = tuple1.get_date();
    loop {
        tuple1.add_days(1);
        let end_day2 = tuple1.get_date();
        if end_day2 < end_day {
            break;
        } else {
            end_day = end_day2;
        }
    }
    let end = format!("{}-{}-{}", year, get_zoer_val(month), get_zoer_val(end_day));
    (begin, end)
}

#[test]
fn test_last_month() {
    let mounth = last_month();
    println!("mounth: {:?}", mounth);
}
