#[macro_use]
extern crate log;

pub mod arg;
pub mod file;
pub mod mylog;
pub mod string;
pub mod time;
mod my_error;
mod i_result;

pub use my_error::MyError;
pub use i_result::*;


