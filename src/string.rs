/// 截取字符
pub fn sub_string(str: &str, lable: &str) -> (String, String) {
    let len = str.bytes().len();
    let i = str.find(lable).unwrap_or(len);
    if i == len {
        return (str.to_string(), "".to_string());
    }
    let sub = i + lable.bytes().len();
    (str[0..i].to_string(), str[sub..].to_string())
}
