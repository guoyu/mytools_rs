/// 自定义错误类型
pub struct MyError {
    pub msg: String,
}
impl MyError {
    pub fn new(msg: &str) -> MyError {
        MyError {
            msg: msg.to_string(),
        }
    }
    pub fn new_box(msg: &str) -> Box<MyError> {
        let error = self::MyError::new(msg);
        Box::new(error)
    }
    pub fn new_box_err<E: std::fmt::Debug>(e: E) -> Box<MyError> {
        let msg = format!("{:?}", e);
        self::MyError::new_box(&msg)
    }
}
impl std::error::Error for MyError {
    fn description(&self) -> &str {
        self.msg.as_str()
    }
}
impl std::fmt::Display for MyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}
impl std::fmt::Debug for MyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}
