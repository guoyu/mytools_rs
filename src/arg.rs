/// 从启动参数里获取参数 ./etax.exe t=2
/// let t_arg = get_arg("t").parse().unwrap_or(3);
pub fn get_arg(input_k: &str) -> Option<String> {
    let k = format!("{}=", input_k);
    let args = std::env::args().collect::<Vec<_>>();
    let mut filter = args.iter().filter(|v| v.contains(k.as_str()));
    let t = filter.next();
    let string = "".to_string();
    let t = t.unwrap_or(&string);
    let split: Vec<&str> = t.split("=").collect();
    let t = split.get(1).unwrap_or(&"");
    let t = t.to_string();
    let mut arg: Option<String> = {
        if t == "" { get_os_arg(input_k) } else { Some(t) }
    };

    if arg.is_none(){
        if let Ok(v)  = std::env::var(input_k){
            arg = Some(v);
        }
    }

    return arg;
}

#[test]
fn test_get_arg(){
    let arg = get_arg("log");
    println!("{:?}", arg);
}

fn get_os_arg(k: &str) -> Option<String> {
    use std::ffi::OsString;
    let option = std::env::var_os(k);
    let string = option.unwrap_or(OsString::from(""));
    let s = string.to_str().unwrap_or("").to_string();
    return if s == "" { None } else { Some(s) };
}
